/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import lab_8.entities.Comment;
import lab_8.entities.Post;
import lab_8.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    
    protected  Map<Integer, Integer> userLikes()
    {
        Map<Integer, Integer> userLikecount = new HashMap<Integer,Integer>();
        Map<Integer, User>users = DataStore.getInstance().getUsers();
        for(User user : users.values()){
            for(Comment c : user.getComments()){
                int likes = 0; 
                if(userLikecount.containsKey(user.getId()))
                    likes = userLikecount.get(user.getId());
                likes += c.getLikes();
                userLikecount.put(user.getId(), likes);
            }
        }
        return userLikecount;
    }
    
    
   public void userWithMostLikes()
   {
       Map<Integer, Integer> userLikecount = userLikes();
       // Map<Integer, Integer> userLikecount = new HashMap<Integer,Integer>();
        Map<Integer, User>users = DataStore.getInstance().getUsers();
//        for(User user : users.values()){
//            for(Comment c : user.getComments()){
//                int likes = 0; 
//                if(userLikecount.containsKey(user.getId()))
//                    likes = userLikecount.get(user.getId());
//                likes += c.getLikes();
//                userLikecount.put(user.getId(), likes);
//            }
//        }
        int max = 0;
        int maxId = 0;
        for(int id : userLikecount.keySet()){
            if(userLikecount.get(id) >max){
                max = userLikecount.get(id);
                maxId = id;
            }
        }
        System.out.println("User with most likes:" + max+ "\n"+users.get(maxId));
    }
    
    public void getFiveMostLikedComment()
    {
        Map<Integer, Comment> comment = DataStore.getInstance().getComments();
        
        List<Comment> commentList = new ArrayList<>(comment.values());
        Collections.sort(commentList, new Comparator<Comment>()
                 {
                     @Override
                     public int compare(Comment o1, Comment o2)
                     {
                         //so as to get deceding list
                         return o2.getLikes() - o1.getLikes();
                     }          
                 });
        System.out.println("5 most liked comments : ");
        for(int i = 0; i < commentList.size() && i < 5; i++)
         {
             System.out.println(commentList.get(i));      
         }    
    } 
    // 1. find average number of likes per comment(2)
    public void averageLikes1(){
       Map<Integer, User>users = DataStore.getInstance().getUsers();
        int count = 0;
        int likes =0;
       
        for(User user : users.values()){
            for(Comment c : user.getComments()){
                likes +=c.getLikes();                
        }
            count += user.getComments().size(); 
        }
       
        int average = likes / count; 
        System.out.println("The average number of likes is:" + average);
        }
    
        // 1. find average number of likes per comment(1)
    public void averageLikes(){
        Map<Integer, Integer> userLikecount = new HashMap<Integer,Integer>();
        Map<Integer, Integer> userCommentcount = new HashMap<Integer,Integer>();
        Map<Integer, User>users = DataStore.getInstance().getUsers();
        for(User user : users.values()){
            int count =0;
            for(Comment c : user.getComments()){
                int likes = 0;
                if(userLikecount.containsKey(user.getId())){
                    likes = userLikecount.get(user.getId());                   
                }
                likes += c.getLikes();
                userLikecount.put(user.getId(), likes);     
            }
            if(userCommentcount.containsKey(user.getId())){
                    count = userCommentcount.get(user.getId());
                }
                count += user.getComments().size();
                userCommentcount.put(user.getId(), count);
            
        }
        int total = 0;
        int average = 0;
        int number = 0;
        for(int id : userLikecount.keySet()){
            total = total + userLikecount.get(id);    
            }
        for(int id : userCommentcount.keySet()){
            number = number + userCommentcount.get(id);    
            }
        average = total / number;
        
    
        System.out.println("The average number of likes is:" + average);
    }
    
    //2. post with the most liked comment
    public void getPostWithMostLikedComment()
    {
        Map<Integer, Comment> comment = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comment.values());
         Collections.sort(commentList, new Comparator<Comment>()
                 {
                     @Override
                     public int compare(Comment o1, Comment o2)
                     {
                         //so as to get deceding list
                         return o2.getLikes() - o1.getLikes();
                     }          
                 });
         System.out.println("The most liked comment : ");
         System.out.println(commentList.get(0));
         Map<Integer, Post> post = DataStore.getInstance().getPosts();
      
        for(Post p: post.values()){
            for(Comment c : p.getComments()){
                if(commentList.get(0).getId() == c.getId())
                {
                    System.out.println("Post with the most liked comment (postId): "+p.toString());
                }                    
            }
        }
    }

     
 

    // 3. post with most comments
    public void getPostWithMostComment()
    {
        Map<Integer, Post> post = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<>(post.values());
        Collections.sort(postList, new Comparator<Post>()
                 {
                     @Override
                     public int compare(Post o1, Post o2)
                     {
                         //so as to get deceding list
                         return o2.getComments().size() - o1.getComments().size();
                     }          
                 });
        System.out.println("Number of the most comments is:"+""+postList.get(0).getComments().size());
        System.out.println("Post is:"+""+postList.get(0));
       }

    //4.  Top 5 active user based on post

    public void fiveInactUserPost()
    {
        Map<Integer, Post> post = DataStore.getInstance().getPosts();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Integer> userPost = new HashMap<Integer, Integer>();
        List<Post> postList = new ArrayList<>(post.values());

        for (Post p: postList)
        {
            if(userPost.containsKey(p.getUserId()))
            {
                int i = (userPost.get(p.getUserId())+1);
                userPost.put(p.getUserId(), i);
            }
            else
            {
                int j = 1;
                userPost.put(p.getUserId(), j);
            }
        }
        List<Map.Entry<Integer,Integer>> list = new ArrayList<Map.Entry<Integer,Integer>>(userPost.entrySet());
        Collections.sort(list,new Comparator<Map.Entry<Integer,Integer>>() {
            //升序排序

            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o1.getValue().compareTo(o2.getValue());//To change body of generated methods, choose Tools | Templates.
            }

        });
        System.out.println("Top 5 inactive user based on posts:");

        int i = 0;
        for(Map.Entry<Integer,Integer> mapping:list){
            System.out.println(users.get(mapping.getKey()).toString());

            i = i+1;
            if(i == 5){
                break;
            }
        }
    }
    
    // 5. top 5 inactive users based comments
    public void getFiveLeastInactiveUsersOnComments(){
        
        Map<Integer, User> user = DataStore.getInstance().getUsers();
        List<User> userList = new ArrayList<>(user.values());
         Collections.sort(userList, new Comparator<User>()
                 {
                     @Override
                     public int compare(User o1, User o2)
                     {
                         //so as to get deceding list
                         return o1.getComments().size() - o2.getComments().size();
                     }          
                 });
         System.out.println("Top 5 inactive user based on comments: ");
         for(int i = 0; i < userList.size() && i < 5; i++)
         {
             System.out.println(userList.get(i));      
         }    
//
    }
    //Q6
    public void getFiveInactiveUsersOverall(){
        //get userId in post and comment.
        //get the least 5 total number for a userId in both post and comment.
        Map<Integer, Post> post = DataStore.getInstance().getPosts();
        Map<Integer, Comment> comment = DataStore.getInstance().getComments();
        Map<Integer, User> user = DataStore.getInstance().getUsers();
//        Map<Integer, Integer> userMap = new HashMap<>();
        List<Post> postList = new ArrayList<>(post.values());
        List<Comment> commentList = new ArrayList<>(comment.values());
        List<User> userList = new ArrayList<>(user.values());
        
        Map<Integer, Integer> inactiveUser = new HashMap<>();
        int[] count = new int[userList.size()]; 
        for(Post p: postList) 
            count[p.getUserId()]++;
        for(Comment c: commentList)
            count[c.getUserId()]++;
        for(int i=0; i<userList.size(); i++){
            inactiveUser.put(i, count[i]);
        }
        List<Map.Entry<Integer, Integer>> inactiveUserList = new ArrayList<>(inactiveUser.entrySet());
        Collections.sort(inactiveUserList, new Comparator<Map.Entry<Integer, Integer>>(){
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2){
                return o1.getValue() - o2.getValue();
            }
        });

        System.out.println("Top 5 inactive users based on posts and comments");
        int i=0;
        for(Map.Entry<Integer, Integer> userEntry: inactiveUserList){
            System.out.println("User: "+ userEntry.getKey() + " toal posts and comments: " + userEntry.getValue());
            i++;
            if(i>4) break;
        }
             
        
    }
    public void getFiveProactiveUsersOverall(){
        Map<Integer, Post> post = DataStore.getInstance().getPosts();
        Map<Integer, Comment> comment = DataStore.getInstance().getComments();
        Map<Integer, User> user = DataStore.getInstance().getUsers();
//        Map<Integer, Integer> userMap = new HashMap<>();
        List<Post> postList = new ArrayList<>(post.values());
        List<Comment> commentList = new ArrayList<>(comment.values());
        List<User> userList = new ArrayList<>(user.values());
        
        Map<Integer, Integer> proactiveUser = new HashMap<>();
        int[] count = new int[userList.size()]; 
        for(Post p: postList) 
            count[p.getUserId()]++;
        for(Comment c: commentList)
            count[c.getUserId()]++;
        for(int i=0; i<userList.size(); i++){
            proactiveUser.put(i, count[i]);
        }
        List<Map.Entry<Integer, Integer>> proactiveUserList = new ArrayList<>(proactiveUser.entrySet());
        Collections.sort(proactiveUserList, new Comparator<Map.Entry<Integer, Integer>>(){
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2){
                return o2.getValue() - o1.getValue();
            }
        });

        System.out.println("Top 5 inactive users based on posts and comments");
        int i=0;
        for(Map.Entry<Integer, Integer> userEntry: proactiveUserList){
            System.out.println("User: "+ userEntry.getKey() + " toal posts and comments: " + userEntry.getValue());
            i++;
            if(i>4) break;
        }
    }
}
