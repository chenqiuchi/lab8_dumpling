/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analytics;

import assignment_4.entities.*;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Xie
 */
public class DataStore {
    private static DataStore dataStore;
    
    private Map<Integer, Customer> customers;
    private Map<Integer, SalesPerson> salepersons;
    private Map<Integer, Order> orders;
    private Map<Integer, Item> items;
    private Map<Integer, Product> products;
    
    public DataStore(){
        customers = new HashMap<>();
        salepersons = new HashMap<>();
        orders = new HashMap<>();
        items = new HashMap<>();
        products = new HashMap<>();
    }
    
    public static DataStore getInstance(){
        if(dataStore == null)
            dataStore = new DataStore();
        return dataStore;
    }
    
    public static DataStore getDataStore() {
        return dataStore;
    }

    public Map<Integer, Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(Map<Integer, Customer> customers) {
        this.customers = customers;
    }

    public Map<Integer, SalesPerson> getSalepersons() {
        return salepersons;
    }

    public void setSalepersons(Map<Integer, SalesPerson> salepersons) {
        this.salepersons = salepersons;
    }

    public Map<Integer, Order> getOrders() {
        return orders;
    }

    public void setOrders(Map<Integer, Order> orders) {
        this.orders = orders;
    }

    public Map<Integer, Item> getItems() {
        return items;
    }

    public void setItems(Map<Integer, Item> items) {
        this.items = items;
    }

    public Map<Integer, Product> getProducts() {
        return products;
    }

    public void setProducts(Map<Integer, Product> products) {
        this.products = products;
    }
    
    
    
}
