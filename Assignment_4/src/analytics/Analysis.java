
package analytics;

import analytics.DataStore;
import assignment_4.entities.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Analysis {
    
    
    //1. Our top 3 most popular product sorted from high to low.
    public void getCustomerWithMostItems(){
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        Map<Integer, Integer> ordercount = new HashMap<Integer,Integer>();
//        Map<Integer, Integer> itemcount = new HashMap<Integer,Integer>();
        for(Order o : orders.values()){
            int count = 0;
            if(ordercount.containsKey(o.getItem().getProductId())){
                count = ordercount.get(o.getItem().getProductId())+o.getItem().getQuantity();
            }else{
                count = o.getItem().getQuantity();
            }
            ordercount.put(o.getItem().getProductId(), count);
        
        }
        List<Map.Entry<Integer,Integer>> itemcountlist = new ArrayList<Map.Entry<Integer,Integer>>(ordercount.entrySet());
        Collections.sort(itemcountlist,new Comparator<Map.Entry<Integer,Integer>>() {
            //降序排序      

            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
            
        });
        Map<Integer,Product> product = DataStore.getInstance().getProducts();
        System.out.println("");
        System.out.println("Our top 3 most popular product are:");
        int i = 0;
        for(Map.Entry<Integer,Integer> mapping:itemcountlist){ 
            System.out.println(product.get(mapping.getKey()).toString());
               
            i = i+1;
            if(i == 3){
                break;
            }
          }
        }
    
    //2. Our 3 best customers.
    public void get3BestCustomer(){
        Map<Integer, Order>orders = DataStore.getInstance().getOrders();
//        Map<Integer, Integer> ordercount = new HashMap<Integer,Integer>();
        Map<Integer, Integer> orderprice = new HashMap<Integer,Integer>();
        for(Order o : orders.values()){
            int count = 0;
            int price =0;
            if(orderprice.containsKey(o.getCustomerId())){
//                count = ordercount.get(o.getCustomerId())+o.getItem().getQuantity();
                price = orderprice.get(o.getCustomerId())+((o.getItem().getSalesPrice())*(o.getItem().getQuantity()));
            }else{
//                count = o.getItem().getQuantity();
                price = (o.getItem().getQuantity()) * (o.getItem().getSalesPrice());
            }
//            price = count * o.getItem().getSalesPrice();
//            ordercount.put(o.getCustomerId(), count);
            orderprice.put(o.getCustomerId(), price);
        
        }
        List<Map.Entry<Integer,Integer>> orderpricelist = new ArrayList<Map.Entry<Integer,Integer>>(orderprice.entrySet());
        Collections.sort(orderpricelist,new Comparator<Map.Entry<Integer,Integer>>() {
            //降序排序      

            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
            
        });
        Map<Integer,Customer> customer = DataStore.getInstance().getCustomers();
        System.out.println("");
        System.out.println("Our top 3 best customers are:");
        int i = 0;
        for(Map.Entry<Integer,Integer> mapping:orderpricelist){ 
            System.out.println("The Total Price customer spent is:"+ mapping.getValue());
            System.out.println("The"+(i+1)+"customer is:");
            System.out.println(customer.get(mapping.getKey()).toString());
               
            i = i+1;
            if(i == 3){
                break;
            }
          }
    }
    
    //3.our top 3 best sales people.
    public void TopThirSales()
    {
        Map<Integer, SalesPerson> salesPersons = DataStore.getInstance().getSalepersons();
        Map<Integer, Integer> salesPerform = new HashMap<Integer, Integer>();

        for(SalesPerson sp: salesPersons.values())
        {
            int totalPrice = 0;
            for(Order order: sp.getOrder())
            {
                Product p = searchProduct(order.getItem().getProductId());
                totalPrice = totalPrice + (order.getItem().getQuantity()*(order.getItem().getSalesPrice()-p.getMinPrice()));
            }
            salesPerform.put(sp.getSaleId(), totalPrice);
        }
        List<Map.Entry<Integer,Integer>> salesPerformISO = new ArrayList<Map.Entry<Integer,Integer>>(salesPerform.entrySet());
        Collections.sort(salesPerformISO,new Comparator<Map.Entry<Integer,Integer>>() {
            //descding sorting
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });
        
        System.out.println("");
        System.out.println("Our top 3 best sales people.:");
        int i = 0;
        for(Map.Entry<Integer,Integer> mapping : salesPerformISO){ 
            
            System.out.print("The raging "+(i+1)+"sales person is: ");
            System.out.println(salesPersons.get(mapping.getKey()).toString() + " Sales Perform: " + mapping.getValue()); 
            i = i+1;
            if(i == 3){
                break;
            }
          }

    }
    
    //4.Our total revenue for the year 
    
    public void totalRevenue()
    {
        Map<Integer, SalesPerson> salesPersons = DataStore.getInstance().getSalepersons();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        
        int totalRevenue = 0;
        for(SalesPerson sp: salesPersons.values())
        {
            for(Order order: sp.getOrder())
            {
                Product p = searchProduct(order.getItem().getProductId());
                totalRevenue = totalRevenue + (order.getItem().getQuantity()*(order.getItem().getSalesPrice()-p.getMinPrice()));
            }
        }
        
        System.out.println("");
        System.out.println("Our total revenue is: " + totalRevenue);  
            
    }

        public Product searchProduct(int i)
        {
            Map<Integer, Product> products = DataStore.getInstance().getProducts();
            for(Product p : products.values())
            {
                if(i == p.getProductId())
                {
                    return p;
                }
            }
            return null;
        }
    
}