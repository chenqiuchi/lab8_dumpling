/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import java.io.IOException;
import assignment_4.entities.*;
import analytics.*;
import java.util.Map;

/**
 *
 * @author harshalneelkamal
 */
public class GateWay {
    
    DataReader orderReader;
    DataReader productReader;
    Analysis analysis;
    

    
    public GateWay() throws IOException{
        DataGenerator generator = DataGenerator.getInstance();
        orderReader = new DataReader(generator.getOrderFilePath());
        productReader = new DataReader(generator.getProductCataloguePath());
        analysis = new Analysis();

    }
    
    public static void main(String args[]) throws IOException{
        
        GateWay inst = new GateWay();
        inst.readData();
         
//        DataGenerator generator = DataGenerator.getInstance();
//        
//        //Below is the sample for how you can use reader. you might wanna 
//        //delete it once you understood.
//        
//        DataReader orderReader = new DataReader(generator.getOrderFilePath());
//        String[] orderRow;
//        printRow(orderReader.getFileHeader());
//        while((orderRow = orderReader.getNextRow()) != null){
//            printRow(orderRow);
//        }
//        System.out.println("_____________________________________________________________");
//        DataReader productReader = new DataReader(generator.getProductCataloguePath());
//        String[] prodRow;
//        printRow(productReader.getFileHeader());
//        while((prodRow = productReader.getNextRow()) != null){
//            printRow(prodRow);
//        }
    
    }
    
    
//    public static void printRow(String[] row){
//        for (String row1 : row) {
//            System.out.print(row1 + ", ");
//        }
//        System.out.println("");
//    }
    private void readData() throws IOException{
       String[] row;
       while((row = productReader.getNextRow()) != null ){
           generateProduct(row);
       }
       while((row = orderReader.getNextRow()) != null ){
           Item item = generateItem(row);
           Order order = generateOrder(row, item);
           generateCustomer(row, order);
           generateSalesPerson(row, order);
       }
        
        runAnalysis();
    }
    
    private void generateProduct(String[] row){
        int productID = Integer.parseInt(row[0]);
        int minPrice = Integer.parseInt(row[1]);
        int maxPrice = Integer.parseInt(row[2]);
        int targetPrice = Integer.parseInt(row[3]);
        Product p = new Product(productID, minPrice, maxPrice, targetPrice);
        DataStore.getInstance().getProducts().put(productID, p);
    }
    
    private Order generateOrder(String[] row, Item item){
        int orderId = Integer.parseInt(row[0]);
        int supplierId = Integer.parseInt(row[4]);
        int customerId = Integer.parseInt(row[5]);
        Order order = new Order(orderId, supplierId, customerId, item);
        DataStore.getInstance().getOrders().put(orderId, order);
        return order;
    }
    
    private Item generateItem(String[] row){
        int itemId = Integer.parseInt(row[1]);
        int prodcutId = Integer.parseInt(row[2]);
        int salesPrice = Integer.parseInt(row[6]);
        int quantity = Integer.parseInt(row[3]);
        Item i = new Item(itemId, prodcutId, salesPrice, quantity);
        DataStore.getInstance().getItems().put(itemId, i);
        return i;
    }
    
    private void generateCustomer(String[] row, Order order){
        int customerId =  Integer.parseInt(row[5]);
        Map<Integer, Customer> customers = DataStore.getInstance().getCustomers();
        if(customers.containsKey(customerId))
            customers.get(customerId).getOrder().add(order);
        else{
            Customer customer = new Customer(customerId);
            customer.getOrder().add(order);
            customers.put(customerId, customer);
        }
    }
    
    private void generateSalesPerson(String[] row, Order order){
         int saleId = Integer.parseInt(row[4]);
         Map<Integer, SalesPerson> salespersons = DataStore.getInstance().getSalepersons();
         if(salespersons.containsKey(saleId))
             salespersons.get(saleId).getOrder().add(order);
         else{
             SalesPerson salesperson = new SalesPerson(saleId);
             salesperson.getOrder().add(order);
             salespersons.put(saleId, salesperson);
         }
    }
    
    private void runAnalysis(){
    
            analysis.getCustomerWithMostItems();
            analysis.get3BestCustomer();
            analysis.TopThirSales();
            analysis.totalRevenue();
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
     
     
     
    
    
}
