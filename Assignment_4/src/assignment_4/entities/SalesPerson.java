/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author harshalneelkamal
 */
public class SalesPerson {
    private int saleId;
    private List<Order> order;
    
    public SalesPerson(int saleId){
        this.saleId = saleId;
        this.order = new ArrayList<>();
    }

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public List<Order> getOrder() {
        return order;
    }

    public void setOrder(List<Order> order) {
        this.order = order;
    }
    
    @Override
    public String toString() {
        return "SalesPerson{" + "saleId=" + saleId +  "}";
    }
}
