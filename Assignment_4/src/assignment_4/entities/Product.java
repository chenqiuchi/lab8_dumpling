/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.entities;

/**
 *
 * @author harshalneelkamal
 */
public class Product {
    private int productId;
    private int minPrice;
    private int maxPrice;
    private int targetPrice;
    
//    private int availability;
//    private String name;
//    private String description;
//    
    public Product(int productId, int minPrice, int maxPrice, int targetPrice){
        this.productId = productId;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.targetPrice = targetPrice;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public int getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(int targetPrice) {
        this.targetPrice = targetPrice;
    }

//    public int getAvailability() {
//        return availability;
//    }
//
//    public void setAvailability(int availability) {
//        this.availability = availability;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//    
    
    @Override
    public String toString() {
        return "Product{" + "productId=" + productId + ", minPrice=" + minPrice + ", maxPrice=" + maxPrice + ", targetPrice=" + targetPrice + '}';
    }
}
